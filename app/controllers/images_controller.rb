class ImagesController < ApplicationController
  
  # GET /images
  # GET /images.json
  def index
    @claims = ClaimDetail.all
  end
	

   # GET /images/new
  def new
    @claim =  ClaimDetail.new
    4.times do
      image = @claim.images.build
    end
  end
    
  
  # POST /images
  # POST /images.json
  def create
    @claim = ClaimDetail.new(claim_params)
    respond_to do |format|
    if @claim.save!   
        format.html { redirect_to root_path, flash[:success] = "Data saved." }
    else
     format.html { redirect_to root_path, flash[:error] = "Sorry!! Something went wrong." }
    end
   end
  end
	

  # GET /images/1
  # GET /images/1.json
  def show
    @claim = ClaimDetail.find_by(id: params[:id])
  end
     
  private
	# Never trust parameters from the scary internet, only allow the white list through.
  def claim_params
    params.require(:claim_detail).permit(:id,:title, :_destroy, images_attributes: [:id, :detail, :file , :_destroy] )
  end

end
